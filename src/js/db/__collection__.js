'use strict';
var Model = require('./__model__.js')

class Collection {

    constructor(name, models, options) {
        var me = this
        options = options || {}
        me.name = name
        assert(_.isArray(models) || !models, "Document needs list to be initialized")
        me.replaceWith(models, options) // set all the first time
    }

    add(data, options) {
        var me = this
        if(data.constructor === Array) {
            var temp_data = _.map(data, function (model) {
                if (model instanceof Model) {
                    return model
                } else {
                    return new Model(me.name, model)
                }
            })
            me.coll = me.coll.concat(temp_data)
        } else {
            var model = new Model(me.name, data)
            me.coll.push(model)
        }
        Utils.recordDataChange(options)
    }

    size() {
        var me = this
        return me.coll.length
    }

    toJSON() {
        var me = this
        return _.map(me.coll, function (model) {
            return model.toJSON()
        })
    }

    where(fields) {
        var me = this
        return new Collection(me.name+"_search", _.filter(me.coll, function (model) {
            return _.every(fields, function (val, key) {
                return model.get(key) === val
            })
        }), {silent: true})
    }
    
    findWhere(fields) {
        var me = this
        return _.first(_.filter(me.coll, function (model) {
            return _.every(fields, function (val, key) {
                return model.get(key) === val
            })
        }))
    }

    get(id) {
        var me = this
        return me.findWhere({id: id})
    }

    remove(id, options) {
        var me = this;
        me.coll = _.filter(me.coll, function (item) {
            return item.get("id") !== id
        })
        Utils.recordDataChange(options)
    }

    replaceWith(new_models_data, options) {
        var me = this
        me.coll = _.map(new_models_data, function (model) {
            if (model instanceof Model) {
                return model
            } else {
                return new Model(me.name, model)
            }
        })
        Utils.recordDataChange(options)
    }

}

module.exports = Collection
