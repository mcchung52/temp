'use strict';
class Model {
    constructor(name, data) {
        var me = this
        me.name = name
        assert(_.isPlainObject(data) || !data, "Document needs json to be initialized")
        me.data = data || {}
    }

    get(key) {
        var me = this
        return me.data[key]
    }

    // Two possible ways to call:
    // 1. object.set({key: value}, options)
    // 1. object.set(key, value, options)
    set(arg1, arg2, arg3) {
        var me = this
        // fuck javascript for not having overloading
        var options = {}
        if (_.isPlainObject(arg1)) {
            // if json passed in
            var data = arg1
            options = arg2
            _.each(data, function(value, key) {
                me.data[key] = value
            })
        } else {
            // if key, value passed in
            var key = arg1
            var value = arg2
            options = arg3
            me.data[key] = value
        }
        Utils.recordDataChange(options)
    }

    replaceWith(new_data, options) {
        var me = this
        me.data = new_data
        Utils.recordDataChange(options)
    }

    toJSON() {
        var me = this
        return me.data
    }

}


module.exports = Model
