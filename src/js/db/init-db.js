const DB_SCHEMA = {
    singleton: {
        tenant: {},
        user: {},
    },
    collection: {
        provider: null,
    },

}
function initialise(refresh_app) {
    _.map(DB_SCHEMA.singleton, function (data, modelName) {
        if (_.keys(DB).indexOf(modelName) < 0) {
            DB.register_singleton(modelName, data)
            console.log("INITIATED ", modelName, "with data=", data)
        }
    })
    _.map(DB_SCHEMA.collection, function (data, collectionName) {
        if (_.keys(DB).indexOf(collectionName) < 0) {
            DB.register_collection(collectionName, data)
            console.log("INITIATED ", collectionName)
        }
    })
    EventEmitter.on("all", refresh_app)
}

module.exports = initialise
