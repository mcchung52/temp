var Model = require('./__model__')
var UrlMappings = {
    tenant: "/api/entity/v1/tenant/",
};
var Sync = {
    load: function (data) {
        _.map(data, function (modelData, modelName) {
            if (DB[modelName] instanceof Model) {
                DB[modelName].replaceWith(_.first(modelData))
            } else {
                DB[modelName].replaceWith(modelData)
            }
        })
    },
    runCallback: function (counter, size, callback) {
        counter++;
        if (counter === size) {
            if (callback) {
                callback();
            }
        }
        return counter;
    },
    loadData: function (table_data, callback) {
        var counter = 0;
        var me = this
        _.map(table_data, function (filter, table_name) {
            var string = "{" + table_name + ":" + JSON.stringify(filter) + "}";
            if (!Cache.isLoaded(string)) {
                // If the query string is not already loaded
                console.log("running for = " + string)
                Utils.get_request(UrlMappings[table_name], filter, function (response) {
                    var data = {};
                    data[table_name] = response;
                    //Add data to the DB
                    me.load(data);
                    //Add the query to the Cache
                    Cache.addData(string);
                    counter = me.runCallback(counter, _.size(table_data), callback)
                });
            }
            else {
                counter = me.runCallback(counter, _.size(table_data), callback)
            }


        });
    }
}

module.exports = Sync
