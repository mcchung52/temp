var DB = {}

var Collection = require('./__collection__.js')
var Model = require('./__model__.js')


DB.register_singleton = function (name, data) {
    DB[name] = new Model(name, data)
}
DB.register_collection = function (name, data) {
    DB[name] = new Collection(name, data)
}

DB.initialise = require('./init-db')
DB.toJSON = function () {
    var keys = _.keys(DB)
    var data_keys = _.filter(keys, function (key) {
        return DB[key].toJSON // so we don't get all the other random stuff
    })
    return _.object(_.map(data_keys, function (key) {
        return [key, DB[key].toJSON()]
    }))
}

module.exports = DB
