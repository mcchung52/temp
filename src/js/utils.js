var moment = require('moment');
var Utils = {
    app_root : "/app",
    app_name: "Maximid",
    ajax_setup: function(){
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie != '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }
                if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                    // Only send the token to relative URLs i.e. locally.
                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
                }
            }
        });

    },
    recordDataChange: function (options) {
        options = options || {}
        console.log("event")
        if (!options.silent) {
            EventEmitter.emit("all")
        }
    },
    moment: function (isotimestring) {
        // returns a moment object that has been localized
        var convertedLocal = moment.utc(isotimestring).toDate();
        return moment(convertedLocal)
    },
    assert: function (predicate) {
        if (!predicate) {
            throw "Assertion Failed";
        }
    },
    get_request: function (endpoint, data, callback) {
        var me = this;
        var response;
        var endpoint_hit = endpoint;
        // TODO: we can use .query instead of this
        if (!_.isEmpty(data)) {
            endpoint_hit += "?" + me.makeQueryString(data)
        }
        Superagent
            .get(endpoint_hit)
            .end(function (res) {
                response = JSON.parse( res.text );
                if (callback) {
                    callback(response)
                }
            })

    },
    was_success: function (server_res) {
        return server_res && server_res.success === true;
    },
    failure_reason: function (server_res) {
        return (server_res && server_res.reason) || "";
    },
    makeQueryString: function (data) {
        var qs;
        var first = true;
        _.map(data, function (val, key) {
            if (!first) {
                qs += "&" + key + "=" + val;
            } else {
                qs = key + "=" + val;
            }
            first = false;
        })
        return qs;
    },
    format_unknown_data: function (data, name) {
        if (name.indexOf("date") >= 0) {
            data = Utils.moment(data).format('MMMM D, YYYY');
        }
        if (typeof(data) === "boolean") {
            data = data ? "True" : "False";
        }
        return data
    },
    custom_status_order: function (collection, order) {
        return (
            _.sortBy(collection, function (item) {
                var rank = order;
                return rank[item.get("status")];
            })
        )
    },
    getPhotonImageUrl: function (img_url) {
        var cleaned_url = img_url.replace("http://", "").replace("https://", "");
        var NUMBER_CACHING_SERVERS = 3
        var hash_code = Math.abs(Utils.adler32(cleaned_url))
        var server_num = hash_code % NUMBER_CACHING_SERVERS
        var cached_url = "//" + "i" + server_num + ".wp.com/" + cleaned_url;
        if (img_url.indexOf("https://") >= 0) {
            cached_url += "?ssl=1"
        }
        // Format: http://i0.wp.com/REMOTE_IMAGE_URL
        return cached_url
    },
    get_profile_pic: function(profile_pic){
        return profile_pic || "/static/app/theme/img/defaultpic.jpg";
    }
}
module.exports = Utils;