const dummy_data = [
    {
        name: "Registrations",
        value: 886200,
        style: "info",
        secondary:{
            name: "Total Registrations",
            percent: 50
        }
    },
    {
        name: "SignIns",
        value: 88200,
        style: "success",
        secondary:{
            name: "Total SignIns",
            percent: 40
        }
    },
    {
        name: "Downloads",
        value: 8560,
        style: 'primary',
        secondary:{
            name: "Total Downloads",
            percent: 10
        }
    },
    {
        name: "Drop Rate",
        value: 200,
        style: "warning",
        secondary:{
            name: "Total Drop",
            percent: 20
        }
    },
]
var HomePage = React.createClass({
    render: function () {
        var me = this;
        return (
                <div className="row">
                    {_.map(dummy_data, function(data,i ){
                        return <Widgets.SmallWidget key={i} data={data}/>
                    })}
                </div>
        )
    }
});

module.exports = HomePage