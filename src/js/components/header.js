var Header = React.createClass({
    render: function () {
        var me = this;
        var user_type = me.props.userType;
        return (
            <div className="row wrapperborder-bottom white-bg page-heading">
                <br />
                <Row >
                {user_type !== "candidate" ? (
                    <div>
                        <Col md={12} className="text-center">
                            <br />
                            <ButtonLink to="add-candidate" bsStyle="primary" bsSize="large" style={{fontSize: "25px"}}>
                                <i className="fa fa-plus"></i>
                                Add a Candidate</ButtonLink>
                        </Col>

                        <Col md={12} className="text-center" >
                            <br/>
                            <h2>- OR -</h2>
                            <br/>
                        </Col>
                    </div>
                ) : ""}
                    <Col md={12} className="text-center">
                        <JobSearchForm searchJob={me.props.searchJob}/>
                    </Col>
                </Row>
            </div>
        )
    }
});

var JobSearchForm = React.createClass({
    mixins: [React.addons.LinkedStateMixin],
    getInitialState: function () {
        return {
            filterLocations: [],
            locations: [],
            locationInput: ''
        }
    },
    componentDidMount: function () {
        var me = this;
        me.setState({locations: _.uniq(_.pluck(DB.Jobs.toJSON(), 'location'))});
    },
    locationChange: function (e) {
        var me = this;
        me.setState({locationInput: e.target.value});
        var locationInput = e.target.value;
        var locations = me.state.locations;
        if (locationInput.trim() !== '') {
            var filter = new RegExp('^' + locationInput, 'i');

            me.setState({
                filterLocations: _.filter(locations, function (location) {
                    return filter.test(location);
                })
            })
            console.log(me.state.filterLocations);
        } else {
            me.setState({filterLocations: []});
        }

    },
    selectLocation: function (location) {
        var me = this;
        me.setState({filterLocations: []});
        me.setState({locationInput: location});
    },
    submitForm: function(e){
        var me = this;
        e.preventDefault();
        var data = {};
        data['company_title'] = me.refs.company_title.getValue();
        data['location'] = me.refs.location.getValue();
        me.props.searchJob(data);
    },
    render: function () {
        var me = this;

        return (
            <div className="search-form col-md-6 col-md-offset-3" >
                <form ref="jobSearchForm" onSubmit={me.submitForm}>
                    <div className="input-group">
                        <Row>
                            <Col md={5}>
                                <Input ref="company_title" type="text" placeholder="Enter Title or Company" name="company_title" className="form-control input-lg col-md-6" />
                            </Col>
                            <Col md={5} className="location-dropdown">
                                <Input ref="location" type="text" ref="location" value={me.state.locationInput} placeholder="Enter Location" onChange={me.locationChange} name="location" className="form-control input-lg col-md-4" />
                        { _.isEmpty(me.state.filterLocations) ? "" : <DropDownLocations locations={me.state.filterLocations} onLocationSelect={me.selectLocation} />}
                            </Col>
                            <Col md={2}>
                                <div className="input-group-btn">
                                    <button className="btn btn-lg btn-primary" type="submit">
                                        Search
                                    </button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </form>
            </div>
        )
    }
});

var DropDownLocations = React.createClass({
    handleClick: function (e) {
        var me = this;
        var onLocationSelect = me.props.onLocationSelect;

        var value = e.target.getAttribute('data-value');

        onLocationSelect(value);
    },
    render: function () {
        var me = this;
        var locations = me.props.locations;
        return (
            <ListGroup>
            {
                _.map(locations, function (location) {
                    return <div className="list-group-item" onClick={me.handleClick} data-value={location}>
                        {location}
                    </div>
                })
                }
            </ListGroup>

        )
    }
})


module.exports = Header;
