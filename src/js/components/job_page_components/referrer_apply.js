var ReferrerApply = React.createClass({
    mixins: [Router.State, Router.Navigation],
    render: function() {
        var me = this;
        return (
            <div>
                <ModalTrigger modal={<ShowCandidates referred={me.props.applied} job_id={me.getParams().id}/>}>
                    <button type="button" className="btn btn-primary btn-sm btn-block">
                    <i className="fa fa-envelope"></i> Refer</button>
                </ModalTrigger>
            </div>
        )
    }
});

var ShowCandidates = React.createClass({
    getInitialState: function(){
        return {
            loading: true,
            candidates: []
        }
    },
    componentDidMount: function(){
        var me = this;
        me.setState({loading: true})
        Superagent.get('/data/candidates').end(function(res) {
            var data = res.text;
            me.setState({loading: false, candidates: data})
        })
    },
    refer_success: function(){
        var me = this;
        me.props.referred();
        me.props.onRequestHide();
    },
    render: function(){
        var me = this;
        if(me.state.loading){
            return <div>Loading</div>
        }
        return (
            <Modal {...this.props} bsStyle='primary' title='Choose Candidate'
                animation={false}>
                <div className='modal-body'>
                {
                    _.map(eval(me.state.candidates), function (candidate) {
                        return <CandidateRow job_id={me.props.job_id} candidate={candidate} success={me.refer_success} />
                    })
                    }
                </div>
                <div className='modal-footer'>
                    <Button onClick={this.props.onRequestHide}>Close</Button>
                </div>
            </Modal>
        )
    }
});

var CandidateRow = React.createClass({
    mixins: [Router.State, Router.Navigation],
    refer_candidate: function(){
        var me = this;
        var job_id = me.props.job_id;
        var candidate_id = me.props.candidate.id;
        Utils.ajax_setup();
        $.post( '/data/apply_job', {
            job_id: job_id,
            candidate_id: candidate_id
        }).done(function( data ) {
            console.log("Candidate Referred");
            me.props.success();
        });
    },
    render: function () {
        var me = this;
        var candidate = me.props.candidate;
        console.log(candidate)
        return (
            <Panel>
            <Row className="text-center">
                <Col md={2}>
                    <img src={Utils.get_profile_pic(candidate.profile_pic)} width="50px" style={{borderRadius: "50px"}}/>
                </Col>
                <Col md={8}>
                    <strong>{candidate.name} </strong>
                    ({candidate.email})
                </Col>
                <Col md={2}>
                    <Button bsStyle="info" onClick={me.refer_candidate}>Refer</Button>
                </Col>

            </Row>
                </Panel>
        )
    }
});
module.exports = ReferrerApply;