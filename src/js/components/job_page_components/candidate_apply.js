var CandidateApply = React.createClass({
    mixins: [Router.State, Router.Navigation],
    refer: function(){
        var me = this;
        //Refer endpoint hit here
        Utils.ajax_setup();
        $.post( '/data/apply_job', {
            job_id: me.getParams().id
        }).done(function( data ) {
                console.log("Candidate Applied");
                me.props.applied();
            });
    },
    render: function() {
        var me = this;
        return (
            <div>
                <button type="button" className="btn btn-primary btn-sm btn-block" onClick={me.refer}>
                    <i className="fa fa-envelope"></i> Apply</button>
            </div>
        )
    }
});

module.exports = CandidateApply;