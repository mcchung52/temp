var SideBarProfile = React.createClass({
    getInitialState: function () {
        return {
            loading: true
        }
    },
    render: function () {
        var me = this;
        return (
            <li className="nav-header">
                <div className="dropdown profile-element">
                    <span>
                        <img alt="image" className="img-circle" style={{  width: "55px"}}
                             src={"https://d2sy47owewtoz3.cloudfront.net/static/landing/images/profile/rishabh.jpeg"}/>
                    </span>
                    <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                        <span className="clear">
                            <span className="block m-t-xs">
                                <strong className="font-bold">
                                    {"Rishabh Mehan"}
                                </strong>
                            </span>
                        </span>
                        <span>
                            {"Administrator"}
                        </span>
                    </a>
                </div>
                <div className="logo-element">
                    MX+
                </div>
            </li>
        )
    }
})

module.exports = SideBarProfile;