var SideBarProfile = require('./sidebar_profile');

var SideBar = React.createClass({
    render: function () {
        var me = this;
        return (
            <nav key="sidebar" className="navbar-default navbar-static-side" role="navigation">
                <div className="sidebar-collapse">
                    <ul className="nav" id="side-menu">
                        <SideBarProfile />
                        <li>

                            <Link to="index">
                                <i className="fa fa-th-large"></i>
                                <span className="nav-label">Dashboards</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="customers">
                                <i className="fa fa-gear"></i>
                                <span className="nav-label">Setup</span>
                            </Link>
                        </li>
                        <li>
                            <a href="#"><i className="fa fa-sitemap"></i>
                                <span className="nav-label">Manage</span>
                                <span className="fa arrow"></span></a>
                            <ul className="nav nav-second-level ui-sortable collapse" aria-expanded="true">
                                <li>
                                    <Link to="api_keys">
                                        <i className="fa fa-key"></i>
                                        API Keys &amp; SDK
                                    </Link>
                                </li>
                                <li>
                                    <a href="providers.html">
                                        <i className="fa fa-lock"></i>
                                        Providers
                                    </a>
                                </li>
                                <li>
                                    <a href="userrepositories.html">
                                        <i className="fa fa-users"></i>
                                        User Repositories
                                    </a>
                                </li>
                                <li><Link to="customers">
                                    <i className="fa fa-institution"></i>
                                    Customers</Link>
                                </li>
                                <li>
                                    <a href="#">
                                        <i className="fa fa-sitemap"></i>
                                        Policies
                                        <span className="fa arrow">
                                    </span></a>
                                    <ul className="nav nav-third-level collapse ui-sortable">
                                        <li>
                                            <a href="policies.jsp?ttype=auth"><i className="fa fa-users"></i>User
                                                Eligibility</a>
                                        </li>
                                        <li>
                                            <a href="policies.jsp?ttype=register"><i className="fa fa-key"></i>Provider
                                                Policies</a>
                                        </li>
                                        <li>
                                            <a href="policies.jsp?ttype=Application"><i className="fa fa-thumbs-up"></i>Application
                                                Policies</a>
                                        </li>
                                        <li>
                                            <a href="policies.jsp?ttype=all"><i className="fa fa-shield"></i>Assurance
                                                Levels</a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link to="index">
                                <i className="fa fa-dollar"></i>
                                <span className="nav-label">Billing and Usage</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="index">
                                <i className="fa fa-bar-chart-o"></i>
                                <span className="nav-label">{"Insights & Analytics"}</span>
                            </Link>
                        </li>
                    </ul>

                </div>
            </nav>
        )
    }
});
module.exports = SideBar;