var Footer = React.createClass({
    render: function () {
        return (
            <div className="footer">
                <div>
                    <strong>Copyright</strong> {Utils.app_name} &copy; 2016-2017
                </div>
            </div>
        );
    }
});

module.exports = Footer;