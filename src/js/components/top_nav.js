/**
 * Created by hangvirus on 3/29/15.
 */
var TopNav = React.createClass({
    collapseNav: function(){
        SmoothlyMenu()
        var body = $("body");
        body.toggleClass('mini-navbar')
        SmoothlyMenu();

    },
    render: function () {
        var me = this;
        return (
            <div className="row border-bottom">
                <nav className="navbar navbar-static-top" role="navigation" style={{marginBottom: 0}}>
                    <div className="navbar-header">
                        <a className="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#" onClick={me.collapseNav}>
                            <i className="fa fa-bars"></i>
                        </a>
                    </div>
                    <ul className="nav navbar-top-links navbar-right">
                        <li>
                            <a href="/auth/logout">
                                <i className="fa fa-sign-out"></i>
                                Log out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
});

module.exports = TopNav;