/**
 * Created by hangvirus on 3/29/15.
 */

/**
 * TODO - Pass company in here/ JOB id
 */
var CompanyWidget = React.createClass({
    render: function () {
        var me = this;
        return (
            <div className="col-md-2" style={{padding: 10}}>
                <div className="ibox-content text-center">
                    <h1>Nicki Smith</h1>
                    <div className="m-b-sm">
                        <img alt="image" className="img-circle" src="/static/app/theme/img/a8.jpg" />
                    </div>
                    <p className="font-bold">Consectetur adipisicing</p>

                    <div className="text-center">
                        <a className="btn btn-xs btn-white">
                            <i className="fa fa-thumbs-up"></i>
                            Like </a>
                        <a className="btn btn-xs btn-primary">
                            <i className="fa fa-heart"></i>
                            Love</a>
                    </div>
                </div>
            </div>
        )
    }
});
module.exports = CompanyWidget;