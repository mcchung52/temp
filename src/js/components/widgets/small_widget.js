var AnotherWidget = React.createClass({
    render: function () {
        var me = this;
        var data = me.props.data
        return (
            <div className="col-lg-3 ">
                <div className="ibox float-e-margins">
                    <div className="ibox-title">
                        <span className={"label pull-right label-"+data.style}>Monthly</span>
                        <h5>{data.name}</h5>
                    </div>
                    <div className="ibox-content">
                        <h1 className="no-margins">{numeral(data.value).format("0,0")}</h1>

                        <div className="stat-percent font-bold text-success">{data.secondary.percent}% <i
                            className="fa fa-bolt"></i>
                        </div>
                        <small>{data.secondary.name}</small>
                    </div>
                </div>
            </div>
        )
    }
});
module.exports = AnotherWidget;