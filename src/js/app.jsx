'use strict';
var global = require('./global.jsx').load()
var Home = require('./pages/Home/dashboard.jsx')
var SideBar = require('./components/sidebar')
var TopNav = require('./components/top_nav')
var Footer = require('./components/footer')
var APIKeys = require('./pages/Manage/API_Keys/api_keys')
var Customers = require('./pages/Manage/Customers/customers')
var NotFound = React.createClass({
    render: function () {
        return (
            <div>
                URL Not Found
            </div>
        )
    }
});
var AppHome = React.createClass({
    render: function () {
        return (
            <div id="wrapper">
                <SideBar/>
                <div id="page-wrapper" className="gray-bg">
                    <TopNav />

                    <div className="row">
                        <div className="col-lg-12">
                            <div className="wrapper wrapper-content">
                                <RouteHandler />
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
});
var routes = (
    <Route name="boilerplate" path="/" handler={AppHome}>
        {/* Home */}
        <DefaultRoute handler={Home}/>
        <Route name="index" path="/" handler={Home}/>
        <Route name="api_keys" handler={APIKeys}/>
        <Route name="customers" handler={Customers}/>
        <NotFoundRoute handler={NotFound}/>
    </Route>
);
Router.run(routes, Router.HistoryLocation, function (Handler) {
    function refresh_react_app() {
        console.log("RERENDER")
        React.render(<Handler/>, document.body);
    }
    refresh_react_app();
    DB.initialise(refresh_react_app)
});