var gulp = require('gulp');
var concat = require('gulp-concat');
var browserify = require('gulp-browserify');
var less = require('gulp-less');
var runSequence = require('run-sequence');

function handleError(e) {
    console.error(e.message + '\n  in ' + e.fileName)
}
gulp.task('html', function() {
    return gulp.src('./index.html')
        .pipe(gulp.dest('build'))
})
gulp.task('browserify', function(){
    gulp.watch(['src/js/**/*.*','src/js/*.*'], ['js-build']);

    return gulp.src('src/js/app.jsx')
        .pipe(browserify({transform: 'reactify'}))
        .on('error', handleError)
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./build/app'))
});


gulp.task('unify-js', function() {
    return gulp.src([
        'src/theme/js/jquery-2.1.1.js',
        'src/theme/js/bootstrap.js',
        'src/theme/js/plugins/metisMenu/jquery.metisMenu.js',
        'src/theme/js/plugins/slimscroll/jquery.slimscroll.min.js',
        'src/theme/js/inspinia.js',
        'build/app/app.js'])
        .on('error', handleError)
        .pipe(concat("app.js")) // Concat and define filename
        .pipe(gulp.dest('./build/app'))
});
gulp.task('default',['js-build', 'less-dev', 'html']);

gulp.task('js-build', function(){
    return runSequence('browserify','unify-js');
})
gulp.task('less-dev', function () {
    gulp.watch(['**/less/**/*.less'], ["less-dev"]);
    return gulp.src(['src/less/*.less'])
        .pipe(less())
        .on('error', handleError)
        .pipe(gulp.dest('./build/app/less'));
});

gulp.task('watch', function() {
    gulp.watch('src/js/*.*', ['default']);
})
